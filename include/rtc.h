#ifndef __RTC_H__
#define __RTC_H__

#include <type/urtc.h>

extern void rtc_i2c_io(uint8_t addr,
                       const uint8_t *wptr,
                       uint8_t wlen,
                       uint8_t *rptr,
                       uint8_t rlen);

void rtc_init(void);
void rtc_read(urtc_t *rtc, urtc_bits_t fields);
void rtc_write(const urtc_t *rtc, urtc_bits_t fields);

#endif /* __RTC_H__ */
