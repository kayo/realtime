librtc.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += librtc
librtc.INHERIT := firmware libprimitive
librtc.CDIRS := $(librtc.BASEPATH)include
librtc.SRCS := $(addprefix $(librtc.BASEPATH)src/,\
  ds3231.c)
