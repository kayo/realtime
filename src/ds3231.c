#include <stddef.h>
#include <rtc.h>
#include "private/ds3231_def.h"

typedef struct {
  uint8_t addr;
  ds3231_time_reg_t reg;
} ds3231_io_t;

void rtc_init(void) {
  
}

void rtc_read(urtc_t *rtc, urtc_bits_t fields) {
  ds3231_io_t io;
  
  io.addr = DS3231_REG_TIMEVAL;

  /* read date-time from periphery */
  rtc_i2c_io(DS3231_I2C_ADDRESS,
             &io.addr, sizeof(io.addr),
             (uint8_t*)&io.reg, sizeof(io.reg));
  
  if (fields & udate_year) rtc->date.year   = ds3231_from_reg(io.reg.year) + 2000;
  if (fields & udate_mon)  rtc->date.month  = ds3231_from_reg(io.reg.month);
  if (fields & udate_date) rtc->date.date   = ds3231_from_reg(io.reg.date);
  
  if (fields & utime_hour) rtc->time.hour   = ds3231_from_reg_24h(io.reg.hours);
  if (fields & utime_min)  rtc->time.minute = ds3231_from_reg(io.reg.minutes);
  if (fields & utime_sec)  rtc->time.second = ds3231_from_reg(io.reg.seconds);
}

void rtc_write(const urtc_t *rtc, urtc_bits_t fields) {
  ds3231_io_t io;
  
  io.addr = DS3231_REG_TIMEVAL;

  /* read date-time from periphery */
  rtc_i2c_io(DS3231_I2C_ADDRESS,
             &io.addr, sizeof(io.addr),
             (uint8_t*)&io.reg, sizeof(io.reg));

  if (fields & udate_year) io.reg.year    = ds3231_to_reg(rtc->date.year - 2000);
  if (fields & udate_mon)  io.reg.month   = ds3231_to_reg(rtc->date.mon);
  if (fields & udate_date) io.reg.date    = ds3231_to_reg(rtc->date.date);

  /* fix day of week */
  if (fields & (udate_year & udate_mon & udate_date)) {
    io.reg.day = udate_week_day(&rtc->date);
  }
  
  if (fields & utime_hour) io.reg.hours   = ds3231_to_reg_24h(rtc->time.hour);
  if (fields & utime_min)  io.reg.minutes = ds3231_to_reg(rtc->time.min);
  if (fields & utime_sec)  io.reg.seconds = ds3231_to_reg(rtc->time.sec);

  /* write date-time to periphery */
  rtc_i2c_io(DS3231_I2C_ADDRESS,
             (const uint8_t*)&io, sizeof(io),
             NULL, 0);
}
